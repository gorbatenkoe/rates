package com.kravchenko.revolut

import com.kravchenko.revolut.data.CurrencyDataUseCase
import com.kravchenko.revolut.data.dto.Rate
import com.kravchenko.revolut.ui.main.CurrenciesMapper
import com.kravchenko.revolut.ui.main.model.CurrencyPresentationModel
import com.nhaarman.mockitokotlin2.*
import io.reactivex.Observable
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import java.lang.NullPointerException
import java.math.BigDecimal

/**
 * Created by kravchenko
 */
@RunWith(MockitoJUnitRunner::class)
class MainPresenterTest {

    @get:Rule
    val rxSchedulersOverrideRule = RxSchedulersOverrideRule

    @Mock
    lateinit var view: MainContract.View
    @Mock
    lateinit var useCase: CurrencyDataUseCase
    @Mock
    lateinit var mapper: CurrenciesMapper

    private lateinit var presenter: MainPresenter

    private val demoModel = CurrencyPresentationModel(
        0,
        DUMMY_NAME,
        DUMMY_DESCRIPTION,
        DUMMY_VALUE,
        DUMMY_FORMATTED,
        false
    )

    @Before
    fun setUp() {
        presenter = MainPresenter(useCase, mapper)
        presenter.view = view
    }

    @Test
    fun onCurrencyClicked_InvalidPosition_ViewShowsError() {
        ArrangeBuilder().withCurrencies()

        presenter.onCurrencyClicked(-2)

        verify(view).showError()
        verify(mapper, never()).toPresentationModels(any(), any())
    }

    @Test
    fun onCurrencyClicked_ValidPosition_CallsMapper() {
        val models = listOf(demoModel)
        ArrangeBuilder()
            .withCurrencies(models)
            .withRate()

        presenter.onCurrencyClicked(0)

        verify(view, never()).showError()
        verify(mapper).reorder(demoModel.currencyName, models)
    }

    @Test
    fun onCurrencyClicked_ValidPosition_ShowsCurrencies() {
        val models = listOf(demoModel)
        val currencies = listOf(demoModel)
        ArrangeBuilder()
            .withCurrencies(models)
            .withRate()
            .withMapperReorder(currencies)

        presenter.onCurrencyClicked(0)

        verify(view).showCurrencies(currencies)
    }

    @Test
    fun onTextChanged_InvalidPosition_NothingHappens() {
        ArrangeBuilder()
            .withCurrencies()

        presenter.onTextChanged(-1, "")

        verify(view, never()).showCurrencies(any())
        verify(mapper, never()).recalculate(any(), any(), any())
    }

    @Test
    fun onTextChanged_ValidPositionAndEmptyText_NothingHappens() {
        ArrangeBuilder()
            .withCurrencies()

        presenter.onTextChanged(0, "")

        verify(view, never()).showCurrencies(any())
        verify(mapper, never()).recalculate(any(), any(), any())
    }

    @Test
    fun onTextChanged_ValidPositionAndSameText_NothingHappens() {
        ArrangeBuilder()
            .withCurrencies()

        presenter.onTextChanged(0, DUMMY_VALUE.toString())

        verify(view, never()).showCurrencies(any())
        verify(mapper, never()).recalculate(any(), any(), any())
    }

    @Test
    fun onTextChanged_ValidPositionAndTextEndsNonDigit_NothingHappens() {
        ArrangeBuilder()
            .withCurrencies()

        presenter.onTextChanged(0, "3.")

        verify(view, never()).showCurrencies(any())
        verify(mapper, never()).recalculate(any(), any(), any())
    }

    @Test
    fun onTextChanged_ValidPositionAndValid_CallsRecalculate() {
        val models = listOf(demoModel)
        ArrangeBuilder()
            .withCurrencies(models)

        presenter.onTextChanged(0, "0.3")

        verify(mapper).recalculate(models, "0.3", demoModel)
    }

    @Test
    fun onTextChanged_ValidPositionAndValid_ShowsOptions() {
        val models = listOf(demoModel)
        ArrangeBuilder()
            .withCurrencies(models)
            .withMapperCalculate(models)

        presenter.onTextChanged(0, "0.3")

        verify(view).showCurrencies(models)
    }

    @Test
    fun attachView_TriggersUseCase() {
        presenter.attachView(view)

        verify(useCase).build(any())
    }

    @Test
    fun attachView_WithFailedUseCase_ShowsError() {
        presenter = MainPresenter(useCase, mapper)

        presenter.attachView(view)

        verify(view, atLeastOnce()).showError()
    }

    private inner class ArrangeBuilder {

        fun withCurrencies(models: List<CurrencyPresentationModel> = emptyList()): ArrangeBuilder =
            apply {
                presenter.currenciesModels = models
            }

        fun withRate(rate: Rate = Rate()): ArrangeBuilder = apply {
            presenter.rate = rate
        }

        fun withMapperReorder(list: List<CurrencyPresentationModel> = listOf(demoModel)) =
            apply {
                whenever(mapper.reorder(any(), any())).thenReturn(list)
            }

        fun withMapperCalculate(list: List<CurrencyPresentationModel> = listOf(demoModel)) =
            apply {
                whenever(mapper.recalculate(any(), any(), any())).thenReturn(list)
            }
    }

    companion object {
        private const val DUMMY_NAME = "name"
        private const val DUMMY_DESCRIPTION = "desc"
        private const val DUMMY_FORMATTED = "0.2"
        private val DUMMY_VALUE = BigDecimal.ZERO
    }
}
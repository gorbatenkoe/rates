package com.kravchenko.revolut.data

import com.kravchenko.revolut.data.dto.Rate
import com.nhaarman.mockitokotlin2.any
import com.nhaarman.mockitokotlin2.whenever
import io.reactivex.Observable
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.junit.MockitoJUnitRunner
import java.lang.NullPointerException

/**
 * Created by kravchenko
 */
@RunWith(MockitoJUnitRunner::class)
class CurrencyDataUseCaseTest {

    @Mock
    lateinit var baseApi: BaseApi

    private lateinit var useCase: CurrencyDataUseCase

    @Before
    fun build() {
        useCase = CurrencyDataUseCase(baseApi)
    }

    @Test
    fun happyPath() {
        ArrangeBuilder().withSuccess()

        val test = useCase.build(CurrencyDataUseCase.Params("EUR")).test()

        test.assertComplete()
        test.assertNoErrors()
        assert(test.values()[0].base == "EUR")
    }

    @Test
    fun queryFailed() {
        val exception = NullPointerException()
        ArrangeBuilder().withFailedQuery(exception)

        val test = useCase.build(CurrencyDataUseCase.Params("EUR")).test()

        test.assertNotComplete()
        test.assertError(exception)
    }

    private inner class ArrangeBuilder {

        fun withSuccess(): ArrangeBuilder = apply {
            whenever(baseApi.getCurrencies(any())).thenReturn(Observable.just(Rate()))
        }

        fun withFailedQuery(throwable: Throwable): ArrangeBuilder = apply {
            whenever(baseApi.getCurrencies(any())).thenReturn(Observable.error(throwable))
        }
    }
}
package com.kravchenko.revolut

import android.app.Application
import android.content.Context
import android.os.AsyncTask
import android.os.Bundle
import androidx.test.runner.AndroidJUnitRunner
import com.github.tmurakami.dexopener.DexOpener
import io.reactivex.plugins.RxJavaPlugins
import io.reactivex.schedulers.Schedulers

class TestRunner : AndroidJUnitRunner() {

    @Throws(InstantiationException::class, IllegalAccessException::class, ClassNotFoundException::class)
    override fun newApplication(cl: ClassLoader?, className: String?, context: Context?): Application {
        DexOpener.install(this)
        return super.newApplication(cl, "com.kravchenko.revolut.TestApplication", context)
    }

    override fun onStart() {
        RxJavaPlugins.setIoSchedulerHandler { Schedulers.from(AsyncTask.THREAD_POOL_EXECUTOR) }
        super.onStart()
    }

    override fun onDestroy() {
        RxJavaPlugins.reset()
        super.onDestroy()
    }
}
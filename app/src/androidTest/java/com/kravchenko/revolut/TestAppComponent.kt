package com.kravchenko.revolut

import com.kravchenko.revolut.di.ActivityInjectorModule
import com.kravchenko.revolut.di.ApiModule
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        ApiModule::class,
        AndroidSupportInjectionModule::class,
        ActivityInjectorModule::class
    ]
)
interface TestAppComponent : AndroidInjector<SimpleApplication> {

    @Component.Builder
    abstract class Builder : AndroidInjector.Builder<SimpleApplication>() {

        /* These methods are necessary for DaggerMock when running instrumentation tests.*/
        abstract fun apiModule(apiModule: ApiModule): Builder
    }
}

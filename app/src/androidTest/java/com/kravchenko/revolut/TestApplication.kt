package com.kravchenko.revolut

import com.kravchenko.revolut.support.DaggerTestAppComponent
import dagger.android.AndroidInjector
import dagger.android.support.DaggerApplication

class TestApplication : SimpleApplication() {

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {

        return DaggerTestAppComponent
            .builder()
            .create(this) as TestAppComponent
    }
}

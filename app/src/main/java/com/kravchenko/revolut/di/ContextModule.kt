package com.kravchenko.revolut.di

import android.content.Context
import com.kravchenko.revolut.SimpleApplication
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Created by kravchenko on 2019-11-18.
 */
@Module
class ContextModule {

    @Provides
    @Singleton
    fun provideContext(application: SimpleApplication): Context {
        return application.applicationContext
    }
}
package com.kravchenko.revolut.di

import com.kravchenko.revolut.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityInjectorModule {

    @ContributesAndroidInjector
    abstract fun mainActivity(): MainActivity
}

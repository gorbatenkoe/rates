package com.kravchenko.revolut.di

import com.kravchenko.revolut.SimpleApplication
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidSupportInjectionModule::class,
        ApiModule::class,
        ActivityInjectorModule::class,
        ContextModule::class
    ]
)

interface AppComponent : AndroidInjector<SimpleApplication> {

    @Component.Builder
    abstract class Builder : AndroidInjector.Builder<SimpleApplication>()
}
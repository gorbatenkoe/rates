package com.kravchenko.revolut

import androidx.annotation.VisibleForTesting
import com.kravchenko.revolut.data.CurrencyDataUseCase
import com.kravchenko.revolut.data.dto.Rate
import com.kravchenko.revolut.ui.main.CurrenciesMapper
import com.kravchenko.revolut.ui.main.model.CurrencyPresentationModel
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import java.util.concurrent.TimeUnit
import javax.inject.Inject

/**
 * Created by kravchenko on 2019-11-17.
 */
class MainPresenter @Inject constructor(
    private val currencyDataUseCase: CurrencyDataUseCase,
    private val mapper: CurrenciesMapper
) : MainContract.Presenter, MainContract.CurrencyListener {

    @VisibleForTesting
    internal lateinit var view: MainContract.View
    @VisibleForTesting
    internal var currenciesModels: List<CurrencyPresentationModel> = mutableListOf()
    @VisibleForTesting
    internal var rate: Rate = Rate()

    private val compositeDisposable: CompositeDisposable = CompositeDisposable()
    private val getCurrenciesDisposable: Disposable =
        Observable.interval(INITIAL_DELAY, UPDATE_INTERVAL, TimeUnit.SECONDS)
            .flatMap {
                return@flatMap currencyDataUseCase.build(CurrencyDataUseCase.Params(rate.base))
            }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(this::onDataLoaded, this::doOnError)

    override fun attachView(view: MainContract.View) {
        this.view = view
        compositeDisposable.add(getCurrenciesDisposable)
    }

    override fun onCurrencyClicked(position: Int) {
        if (position < 0 || position >= currenciesModels.size) {
            view.showError()
            return
        }
        val currency = currenciesModels[position]
        rate = rate.copy(base = currency.currencyName)
        currenciesModels = mapper.reorder(currency.currencyName, currenciesModels)
        view.showCurrencies(currenciesModels)
    }

    override fun onTextChanged(position: Int, text: String) {
        if (position < 0 || position >= currenciesModels.size) {
            return
        }
        val model = currenciesModels[position]
        val initialText = model.currentValue.toString()
        if (initialText == text || (!text.isBlank() && !text.last().isDigit())) {
            return
        }
        currenciesModels = mapper.recalculate(currenciesModels, text, model)
        view.showCurrencies(currenciesModels)
    }

    override fun onDestroy() {
        compositeDisposable.dispose()
    }

    private fun onDataLoaded(rate: Rate) {
        currenciesModels = mapper.toPresentationModels(
            rate,
            currenciesModels.find { it.currencyName == this.rate.base })
        this.rate = rate
        view.showCurrencies(currenciesModels)
    }

    private fun doOnError(throwable: Throwable) {
        view.showError()
    }

    companion object {
        private const val UPDATE_INTERVAL = 1L
        private const val INITIAL_DELAY = 0L
    }
}
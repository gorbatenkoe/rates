package com.kravchenko.revolut.data.base

import io.reactivex.Observable

interface UseCase<Params, Result> {

    fun build(params: Params): Observable<Result>
}

package com.kravchenko.revolut.data

import com.kravchenko.revolut.data.base.UseCase
import com.kravchenko.revolut.data.dto.Rate
import io.reactivex.Observable
import javax.inject.Inject

/**
 * Usecase to achieve data about currencies
 */
class CurrencyDataUseCase @Inject constructor(
    private val baseApi: BaseApi
) : UseCase<CurrencyDataUseCase.Params, Rate> {

    override fun build(params: Params): Observable<Rate> {
        return baseApi.getCurrencies(params.base)
    }

    class Params(val base: String)
}
package com.kravchenko.revolut.data

import com.kravchenko.revolut.data.dto.Rate
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

interface BaseApi {

    @GET("/latest?")
    fun getCurrencies(@Query("base") base: String): Observable<Rate>
}

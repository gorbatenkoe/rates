package com.kravchenko.revolut.data.dto

import java.math.BigDecimal

data class Rate(val base: String = DEFAULT_BASE, val rates: Map<String, BigDecimal> = emptyMap()) {

    companion object {

        private const val DEFAULT_BASE = "EUR"
    }
}
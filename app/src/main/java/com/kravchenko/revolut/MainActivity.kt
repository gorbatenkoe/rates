package com.kravchenko.revolut

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.kravchenko.revolut.ui.base.PresentationModel
import com.kravchenko.revolut.ui.main.CurrencyAdapter
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject

class MainActivity : AppCompatActivity(), MainContract.View {

    @Inject
    lateinit var presenter: MainPresenter

    private lateinit var currencyAdapter: CurrencyAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        presenter.attachView(this)
        rvRates.layoutManager = LinearLayoutManager(this)
        currencyAdapter = CurrencyAdapter(presenter)
        rvRates.adapter = currencyAdapter
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.onDestroy()
    }

    override fun showCurrencies(currencies: List<PresentationModel>) {
        currencyAdapter.setItems(currencies)
    }

    override fun showError() {
        Toast.makeText(this, R.string.error, Toast.LENGTH_SHORT).show()
    }
}

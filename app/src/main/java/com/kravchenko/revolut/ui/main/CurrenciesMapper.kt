package com.kravchenko.revolut.ui.main

import android.content.Context
import com.kravchenko.revolut.data.dto.Rate
import com.kravchenko.revolut.ui.main.model.CurrencyPresentationModel
import com.kravchenko.revolut.util.applyDefaultScale
import java.math.BigDecimal
import java.text.DecimalFormat
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class CurrenciesMapper @Inject constructor(private val context: Context) {

    private val decimalFormat = DecimalFormat()

    init {
        decimalFormat.maximumFractionDigits = 2
        decimalFormat.minimumFractionDigits = 0
        decimalFormat.isGroupingUsed = false
    }

    fun toPresentationModels(
        rateData: Rate,
        selectedModel: CurrencyPresentationModel?
    ): List<CurrencyPresentationModel> {
        val ratesList = rateData.rates
        if (ratesList.isEmpty()) {
            return emptyList()
        }

        return if (selectedModel == null) {
            toInitPresentationModels(rateData)
        } else {
            toUpdatedPresentationModels(rateData, selectedModel)
        }
    }

    fun reorder(
        base: String,
        models: List<CurrencyPresentationModel>
    ): List<CurrencyPresentationModel> {
        val newOrderedModels = mutableListOf<CurrencyPresentationModel>()
        models.forEach { newOrderedModels.add(it.copy(focused = it.currencyName == base)) }
        return newOrderedModels.sortedBy { !it.focused }
    }

    fun recalculate(
        currencies: List<CurrencyPresentationModel>,
        currentText: String,
        model: CurrencyPresentationModel
    ): List<CurrencyPresentationModel> {
        val newCurrencies = mutableListOf<CurrencyPresentationModel>()
        for (currency in currencies) {
            val newValue =
                if (currency.currencyName == model.currencyName)
                    getNewValueForSelectedModel(currentText)
                else
                    getNewValue(currentText, model, currency.currentValue)
            val currentValueFormatted = decimalFormat.format(newValue.applyDefaultScale())
            newCurrencies.add(
                currency.copy(
                    currentValue = newValue,
                    currentValueFormatted = currentValueFormatted,
                    focused = currency.currencyName == model.currencyName
                )
            )
        }

        return newCurrencies
    }

    private fun getNewValue(
        typedText: String,
        selectedModel: CurrencyPresentationModel,
        currencyCurrentValue: BigDecimal
    ): BigDecimal {
        if (selectedModel.currentValue.toFloat() == 0F) return BigDecimal.ZERO
        val newBaseValue = getNewValueForSelectedModel(typedText)
        return (currencyCurrentValue * newBaseValue) / selectedModel.currentValue
    }

    private fun getNewValueForSelectedModel(
        textFromSelectedModel: String
    ): BigDecimal {
        if (textFromSelectedModel.startsWith("0") && textFromSelectedModel.length == 2) textFromSelectedModel.removePrefix(
            "0"
        )
        if(textFromSelectedModel.isBlank()) return BigDecimal.ZERO
        return BigDecimal(textFromSelectedModel)
    }

    private fun toInitPresentationModels(rateData: Rate): List<CurrencyPresentationModel> {
        val ratesList = rateData.rates
        if (ratesList.isEmpty()) {
            return emptyList()
        }

        val models = mutableListOf<CurrencyPresentationModel>()
        getRateFromBase(rateData.base)?.apply { models.add(this) }

        for (rate in ratesList) {
            val currencyFlagId = getCurrencyFlagId(context, rate.key) ?: continue
            val currencyDescription = getCurrencyDescription(context, rate.key) ?: continue
            val value = rate.value.applyDefaultScale()

            models.add(
                CurrencyPresentationModel(
                    currencyFlagId,
                    rate.key,
                    currencyDescription,
                    value,
                    decimalFormat.format(value)
                )
            )
        }

        return models
    }

    private fun toUpdatedPresentationModels(
        rateData: Rate,
        selectedModel: CurrencyPresentationModel
    ): List<CurrencyPresentationModel> {
        val ratesList = rateData.rates
        val models = mutableListOf<CurrencyPresentationModel>()
        models.add(selectedModel)

        for (rate in ratesList) {
            val currencyFlagId = getCurrencyFlagId(context, rate.key) ?: continue
            val currencyDescription = getCurrencyDescription(context, rate.key) ?: continue
            val value =
                rate.value.applyDefaultScale() * selectedModel.currentValue

            models.add(
                CurrencyPresentationModel(
                    currencyFlagId,
                    rate.key,
                    currencyDescription,
                    value,
                    decimalFormat.format(value)
                )
            )
        }

        return models
    }

    private fun getRateFromBase(base: String): CurrencyPresentationModel? {
        val currencyFlagId = getCurrencyFlagId(context, base) ?: return null
        val currencyDescription = getCurrencyDescription(context, base) ?: return null
        val value = BigDecimal(DEFAULT_RATE_VALUE)
        return CurrencyPresentationModel(
            currencyFlagId,
            base,
            currencyDescription,
            value,
            decimalFormat.format(value)
        )
    }

    private fun getCurrencyDescription(context: Context, name: String): String? {
        val resourceNameString = "currency_description_${name.toLowerCase()}"
        val resourceId = getResourceId(
            context,
            resourceNameString,
            "string"
        ).takeUnless { it == null || it == 0 } ?: return null
        return context.getString(resourceId)
    }

    private fun getCurrencyFlagId(context: Context, name: String): Int? {
        val resourceNameString = "ic_${name.toLowerCase()}"
        return getResourceId(context, resourceNameString, "drawable") ?: return null
    }

    private fun getResourceId(
        context: Context,
        resourceName: String,
        resourceDirectory: String
    ): Int? {
        return try {
            context.resources.getIdentifier(resourceName, resourceDirectory, context.packageName)
        } catch (e: Exception) {
            e.printStackTrace()
            null
        }
    }

    companion object {
        private const val DEFAULT_RATE_VALUE = 1
    }
}

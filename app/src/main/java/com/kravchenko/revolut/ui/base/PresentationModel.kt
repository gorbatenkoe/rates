package com.kravchenko.revolut.ui.base

/**
 * Base class for any presentation layer model.
 */
interface PresentationModel
package com.kravchenko.revolut.ui.main.model

import com.kravchenko.revolut.ui.base.PresentationModel
import java.math.BigDecimal

data class CurrencyPresentationModel(
    val countryFlag: Int,
    val currencyName: String,
    val currencyDescription: String,
    val currentValue: BigDecimal,
    val currentValueFormatted: String,
    val focused: Boolean = false
) : PresentationModel
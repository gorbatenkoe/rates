package com.kravchenko.revolut.ui.delegate

import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.kravchenko.revolut.MainContract
import com.kravchenko.revolut.R
import com.kravchenko.revolut.ui.base.AdapterDelegate
import com.kravchenko.revolut.ui.base.PresentationModel
import com.kravchenko.revolut.ui.main.model.CurrencyPresentationModel
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.i_currency_view.view.*


class CurrencyDelegate : AdapterDelegate {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        listener: MainContract.CurrencyListener
    ): RecyclerView.ViewHolder {
        return WeekDayViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.i_currency_view,
                parent,
                false
            ), listener
        )
    }

    override fun onBindViewHolder(
        item: PresentationModel,
        holder: RecyclerView.ViewHolder
    ) {
        holder as WeekDayViewHolder
        item as CurrencyPresentationModel
        holder.onBind(item)
    }

    internal class WeekDayViewHolder(
        override val containerView: View,
        private val listener: MainContract.CurrencyListener
    ) : RecyclerView.ViewHolder(containerView), LayoutContainer {

        private val textWatcher: TextWatcher = object : TextWatcher {
            override fun beforeTextChanged(
                s: CharSequence,
                start: Int,
                count: Int,
                after: Int
            ) {
            }

            override fun onTextChanged(
                s: CharSequence,
                start: Int,
                before: Int,
                count: Int
            ) {
                listener.onTextChanged(adapterPosition, s.toString())
            }

            override fun afterTextChanged(s: Editable) {}
        }

        fun onBind(
            model: CurrencyPresentationModel
        ) {
            val iconDrawable = ContextCompat.getDrawable(containerView.context, model.countryFlag)
            containerView.ivFlag.setImageDrawable(iconDrawable)
            containerView.tvCurrencyName.text = model.currencyName
            containerView.tvCurrencyDescription.text = model.currencyDescription
            containerView.setOnClickListener { listener.onCurrencyClicked(adapterPosition) }
            containerView.etCurrentValue.removeTextChangedListener(textWatcher)
            containerView.etCurrentValue.setText(model.currentValueFormatted)
            if (model.focused)
                containerView.etCurrentValue.requestFocus()
            containerView.etCurrentValue.setSelection(containerView.etCurrentValue.text.length)
            containerView.etCurrentValue.addTextChangedListener(textWatcher)
        }
    }
}

package com.kravchenko.revolut.ui.base

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.kravchenko.revolut.MainContract

interface AdapterDelegate {

    fun onCreateViewHolder(
        parent: ViewGroup,
        listener: MainContract.CurrencyListener
    ): RecyclerView.ViewHolder

    fun onBindViewHolder(
        item: PresentationModel,
        holder: RecyclerView.ViewHolder
    )
}

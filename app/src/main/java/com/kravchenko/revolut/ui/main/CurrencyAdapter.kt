package com.kravchenko.revolut.ui.main

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.kravchenko.revolut.MainContract
import com.kravchenko.revolut.ui.base.PresentationModel
import com.kravchenko.revolut.ui.delegate.CurrencyDelegate

class CurrencyAdapter(private val listener: MainContract.CurrencyListener) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val items = mutableListOf<PresentationModel>()
    private val adapterDelegate: CurrencyDelegate = CurrencyDelegate()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return adapterDelegate.onCreateViewHolder(parent, listener)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        adapterDelegate.onBindViewHolder(items[position], holder)
    }

    override fun getItemViewType(position: Int): Int {
        return 0
    }

    override fun getItemCount(): Int {
        return items.size
    }

    fun setItems(presentationModelList: List<PresentationModel>) {
        items.clear()
        items.addAll(presentationModelList)
        notifyDataSetChanged()
    }
}

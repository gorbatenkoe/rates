package com.kravchenko.revolut.util

import java.math.BigDecimal

/**
 * Created by kravchenko
 */

fun BigDecimal.applyDefaultScale(): BigDecimal = this.setScale(2, BigDecimal.ROUND_HALF_DOWN)
package com.kravchenko.revolut

import com.kravchenko.revolut.ui.base.PresentationModel
import com.kravchenko.revolut.ui.main.model.CurrencyPresentationModel

/**
 * Created by kravchenko on 2019-11-18.
 */
interface MainContract {

    interface View {

        fun showCurrencies(currencies: List<PresentationModel>)

        fun showError()
    }

    interface Presenter {

        fun attachView(view: View)

        fun onDestroy()
    }

    interface CurrencyListener{

        fun onCurrencyClicked(position: Int)

        fun onTextChanged(position: Int, text: String)
    }
}